require('dotenv').config();
require('../database/db');

const Sequelize = require('sequelize');

// create user model
const User = sequelize.define('user', {
    name: {
      type: Sequelize.STRING,
    },
    username: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });
  
  // create table with user model
    // User.sync()
    //   .then(() => console.log('Oh yeah! User table created successfully'))
    //   .catch(err => console.log('BTW, did you enter wrong database credentials?'));
  
  // To remove password values due to response.
  
  User.prototype.toJSON =  function () {
    var values = Object.assign({}, this.get());
  
    delete values.password;
    return values;
  }
  
  // create some helper functions to work on the database
  const createUser = async ({ name, username, email, password }) => { 
    return await User.create({ name, username, email, password });
  };
  
  const getAllUsers = async () => {
    return await User.findAll();
  };
  
  const getUser = async obj => {
    return await User.findOne({
    where: obj
    });
  };
  
  const deleteUser = async obj => {
    return await User.destroy({
      where: obj
    });
  }
  

  exports.User = User;
  exports.createUser = createUser;
  exports.getAllUsers = getAllUsers;
  exports.getUser = getUser;
  exports.deleteUser = deleteUser;