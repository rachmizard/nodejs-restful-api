require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT;
const usersRoute = require('../nodejs-sequelize-mysql-api/routes/user.route')
const app = express();

app.use(bodyParser.json());

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.json());
app.use('/api', usersRoute);

// start the app
app.listen(port, function() {
  console.log(`Express is running on port : ${port}`);
});