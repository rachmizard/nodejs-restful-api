const isAuthenticated = require("../middleware/isAuthenticated");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, getAllUsers, getUser, createUser, deleteUser } = require("../models/user.model");
const express = require("express");
const router = express.Router();

// add a basic route
router.get('/', function(req, res) {
    res.json({ message: 'Express is up!' });
  });
  
// get all users
router.get('/users', isAuthenticated, async function(req, res) {
    await getAllUsers().then(user => res.json(user)); 
});


// get spesific users
router.get('/users/:id', isAuthenticated, async function(req, res) {
    const { id } = req.params;
    console.log(req.decoded)
    await getUser({ id }).then(user => {
        !user ? res.json({ msg: "No such user found.", user: user }) : res.json({ msg: "Show user went well.", user: user });
    }); 
});

// delete user
router.delete('/users/:id', isAuthenticated, async function(req, res, next){
    const { id } = req.params;
    await getUser({ id }).then(responseUser => {
        deleteUser({ id }).then(user => {
        if(!user){
            res.status(401).json({ msg: "No such user found.", user: responseUser });
        }else{
            res.json({ msg: "user deleted successfully", user: responseUser });
        }
        }).catch((err) => {
        res.json(err)
        });
    });
});

// register routes
router.post('/register', async function(req, res, next) {
    const name = req.body.name;
    const username = req.body.username;
    const email = req.body.email;

    await getUser({ username }).then(user => {
        if(!user){
        var password = bcrypt.hashSync(req.body.password, 4);

        createUser({ name, username, email, password }).then(user =>
            res.json({ user, msg: 'account created successfully' })
        );
        }else{
        res.status(409).json({ msg: "This User is already exists!" });
        }
    });
});

// login route
router.post('/login', async function(req, res, next) { 
    const { username, password } = req.body;
    await getUser({username}).then(user => {
        if (!user) {
        res.status(401).json({ msg: 'No such user found', user });
        }else{
        var passwordIsValid = bcrypt.compareSync(password, user.password);
        
        if (passwordIsValid) { // apabila data password sama dengan user password
            var token = jwt.sign(user.toJSON(), process.env.SECRET_KEY, { expiresIn: 60 * 60 });

            res.json({ message: 'berhasil login', user: req.decoded, token: token });
        } else { // apabila salah password
            res.json({ message: 'password salah' });
        }
        }    
    });
});

// protected route
router.get('/protected', isAuthenticated, function(req, res) {
    res.json({ msg: 'Congrats! You are seeing this because you are authorized', user: req.decoded});
});

router.post('/create-table', function(req, res) {

//   create table with user model
    User.sync()
    .then(response => {
        res.json({  msg: 'Table successfully created', info: response  });
    })
    .catch(err => {
        res.json(err)
    });
})
  
module.exports = router;